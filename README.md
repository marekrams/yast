# YASTN - Yet Another Symmetric Tensor Network

### Python library for differentiable linear algebra with block-sparse tensors, supporting abelian symmetries

Further development has been moved to [YASTN](https://github.com/yastn/yastn)
