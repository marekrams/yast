YAMPS
==============================


Basic concepts
---------------------------------------

.. toctree::
   :glob:

   theory/mps/basics
   theory/mps/basics
   theory/mps/basics

Matrix producs state and operator
---------------------------------------

.. toctree::
   :glob:

   theory/mps/basics
   mps/init
   mps/algebra
   mps/i-o


Algorithms
---------------------

.. toctree::
   :glob:

   mps/algorithms


Expectation values for MPS
---------------------------

.. toctree::
   :glob:

   mps/measurements


Examples
---------------------------

.. toctree::
   :glob:

   examples/mps/mps


Module contents
-----------------

.. automodule:: yamps
   :members:
   :undoc-members:
   :show-inheritance:
