Welcome to YAST's documentation!
=================================

YAST - yet another symmetric tensor

Block-sparse tensor algebra library supporting abelian symmetries
together with MPS implementation powered by YAST.

.. toctree::
   :maxdepth: 2
   :caption: Table of contents:

   yast.tensor
   yast.mps


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
