Inspecting YAST tensors
=======================

Get information about tensor's structure and properties
-------------------------------------------------------

.. autoproperty:: yast.Tensor.s
.. autoproperty:: yast.Tensor.s_n
.. autoproperty:: yast.Tensor.n
.. autoproperty:: yast.Tensor.ndim
.. autoproperty:: yast.Tensor.ndim_n
.. autoproperty:: yast.Tensor.isdiag
.. autoproperty:: yast.Tensor.requires_grad
.. autoproperty:: yast.Tensor.size
.. automethod:: yast.Tensor.show_properties
.. automethod:: yast.Tensor.print_blocks_shape
.. automethod:: yast.Tensor.is_complex
.. automethod:: yast.Tensor.get_rank
.. automethod:: yast.Tensor.get_tensor_charge
.. automethod:: yast.Tensor.get_signature
.. automethod:: yast.Tensor.get_legs
.. automethod:: yast.Tensor.get_blocks_charge
.. automethod:: yast.Tensor.get_blocks_shape
.. automethod:: yast.Tensor.get_shape
.. automethod:: yast.Tensor.get_dtype
