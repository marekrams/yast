YAST
====

What are abelian-symmetric tensors
----------------------------------

.. toctree::
   theory/tensor/basics

API: yast and yast.Tensor
-------------------------

.. toctree::
   :glob:

   tensor/configuration
   tensor/symmetry
   tensor/leg
   tensor/init
   tensor/access
   tensor/algebra
   tensor/output
   tensor/autograd

Examples: basics of usage
-------------------------

.. toctree::
   :glob:

   examples/tensor/*
