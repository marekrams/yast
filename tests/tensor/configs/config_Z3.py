import yast.backend.backend_np as backend
import yast.sym.sym_Z3 as sym
default_device = 'cpu'
default_dtype = 'float64'
fermionic = False
default_fusion: str = 'hard'
force_fusion: str = None
