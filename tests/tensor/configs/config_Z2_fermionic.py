import yast.backend.backend_np as backend
import yast.sym.sym_Z2 as sym
fermionic = True
default_device = 'cpu'
default_dtype = 'float64'
default_fusion: str = 'hard'
force_fusion: str = None
