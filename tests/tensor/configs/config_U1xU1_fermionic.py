import yast.backend.backend_np as backend
import yast.sym.sym_U1xU1 as sym
fermionic = (True, True)
default_device = 'cpu'
default_dtype = 'float64'
default_fusion: str = 'hard'
force_fusion: str = None

