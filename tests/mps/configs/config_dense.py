import yast.backend.backend_np as backend
import yast.sym.sym_none as sym
default_device = 'cpu'
default_dtype = 'float64'
fermionic = False
default_fusion = 'meta'
force_fusion: str = None
